# frozen_string_literal: true

# Create recipes schema in database
# rubocop:disable Metrics/MethodLength
class CreateRecipes < ActiveRecord::Migration[6.0]
  def change
    create_table :recipes do |t|
      t.string :author
      t.decimal :rate
      t.string :difficulty
      t.string :budget
      t.string :prep_time
      t.string :total_time
      t.integer :people_quantity
      t.text :author_tip
      t.string :name
      t.string :image
      t.integer :nb_comments
      t.string :cook_time

      t.timestamps
    end

    create_join_table :tags, :recipes

    create_join_table :ingredients, :recipes
  end
end
# rubocop:enable Metrics/MethodLength
