# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

# rubocop:disable Metrics/BlockLength
ActiveRecord::Schema.define(version: 20_201_003_091_018) do
  create_table 'ingredients', force: :cascade do |t|
    t.string 'name'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['name'], name: 'index_ingredients_on_name', unique: true
  end

  create_table 'ingredients_recipes', id: false, force: :cascade do |t|
    t.integer 'ingredient_id', null: false
    t.integer 'recipe_id', null: false
  end

  create_table 'recipes', force: :cascade do |t|
    t.string 'author'
    t.decimal 'rate'
    t.string 'difficulty'
    t.string 'budget'
    t.string 'prep_time'
    t.string 'total_time'
    t.integer 'people_quantity'
    t.text 'author_tip'
    t.string 'name'
    t.string 'image'
    t.integer 'nb_comments'
    t.string 'cook_time'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  create_table 'recipes_tags', id: false, force: :cascade do |t|
    t.integer 'tag_id', null: false
    t.integer 'recipe_id', null: false
  end

  create_table 'tags', force: :cascade do |t|
    t.string 'name'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['name'], name: 'index_tags_on_name', unique: true
  end
end
# rubocop:enable Metrics/BlockLength
