FROM ruby:2.6

RUN apt-get update && \
  apt-get install -y --no-install-recommends postgresql-client \
  && rm -rf /var/lib/apt/lists/*

EXPOSE 3000
ENV NODE_ENV production
ENV RAILS_ENV production

WORKDIR /usr/src/app
COPY . .

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
  apt-get update -qy && apt-get install -yqq nodejs yarn && \
  bundle install --path vendor/bundle

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
