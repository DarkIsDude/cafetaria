# Cafetaria

What can I eat tonight ?

## Ruby version

ruby 2.6.3p62 (2019-04-16 revision 67580) [universal.x86_64-darwin19]

## System dependencies

Done on MacOS

* `export RAILS_ENV=development`
* `export NODE_ENV=development`

## Configuration

* `bundle install --path vendor/bundle`
* `rails db:migrate RAILS_ENV=development`
* `rails webpacker:install`

## Database initialization

You can import data from the JSON file with

```bash
rails importer:from_json[<path file>]
```

## How to run the test suite

You can check the `.gitlab-ci.yml` file to explose the current pipeline.

## Run

* `rails server`
* `bundle exec ./bin/webpack-dev-server`

## Services (job queues, cache servers, search engines, etc.)

* `rails importer:from_json[<path file>]` to import file and load first data

## Deployment instructions

We use heroku to deploy the application. You can install heroku and then `heroku git:remote -a cafetaria`.

You can finally git push it with `git push heroku master`.

## Improvements

* Add pagination 😈
* Improve pipeline 😇
* Add tests 😍
* Use marmiton API 🚀
* Improve algo (in the importer, clear ingredients to have better matching or use ElasticSearch) 🚤
