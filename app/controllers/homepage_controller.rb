# frozen_string_literal: true

# Generated
class HomepageController < ApplicationController
  def index; end
end
