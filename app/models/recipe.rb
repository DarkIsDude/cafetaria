# frozen_string_literal: true

# Recipe model
class Recipe < ApplicationRecord
  has_and_belongs_to_many :ingredients
  has_and_belongs_to_many :tags

  validates :name, presence: true
end
