export interface Ingredient {
  id: number;
  name: string;
}

export interface Tag {
  id: number;
  name: string;
}

export interface Recipe {
  id: number
  name: string
  author: string
  rate: number
  difficulty: string
  budget: string
  prep_time: string
  total_time: string
  people_quantity: number
  author_tip: string
  image: string
  nb_comments: number
  cook_time: string
  ingredients: Ingredient[]
  tags: Tag[]
}
