import { AppBar, Container, Toolbar, Typography } from '@material-ui/core'
import styled from 'styled-components'
import React, { useState } from 'react'
import SearchBar from './components/SearchBar'
import Result from './components/Result'
import { Ingredient } from './model'

const SpacerContainer = styled(Container)`
  padding: 2rem;
`

export default function App() {
  const [ ingredients, setIngredients ] = useState<Ingredient[]>([])

  const addIngredient = (ingredient: Ingredient) => {
    if (ingredients.filter(i => i.id === ingredient.id).length === 0) {
      setIngredients([ingredient, ...ingredients])
    }
  }

  const deleteIngredient = (ingredient: Ingredient) => {
    setIngredients(ingredients.filter(i => i.id !== ingredient.id))
  }

  return <>
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6">
          Cafetaria
        </Typography>
      </Toolbar>
    </AppBar>
    <SpacerContainer>
      <SearchBar ingredients={ingredients} addIngredient={addIngredient} deleteIngredient={deleteIngredient} />
      {ingredients.length > 0 && <Result ingredients={ingredients} />}
    </SpacerContainer>
  </>
}
