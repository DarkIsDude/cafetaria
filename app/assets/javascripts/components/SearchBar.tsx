import { gql, useQuery } from '@apollo/client'
import { Chip, TextField } from '@material-ui/core'
import React from 'react'
import styled from 'styled-components'
import { Ingredient } from '../model'

interface IngredientsQueryResult {
  ingredients: Ingredient[]
}

interface IngredientsQueryParams {
  search: string
}

const SEARCH_INGREDIENTS = gql`
query Ingredients($search: String!) {
  ingredients(limit: 10, search: $search) {
    id
    name
  }
}
`

const SpacedContainer = styled.div`
  margin-bottom: 1rem;
`

interface Props {
  ingredients: Ingredient[];
  deleteIngredient: (Ingredient) => void;
  addIngredient: (Ingredient) => void;
}

export default function SearchBar(props: Props) {
  const { deleteIngredient, addIngredient, ingredients } = props
  const [ search, setSearch ] = React.useState<string>('')
  const { loading, error, data } = useQuery<IngredientsQueryResult, IngredientsQueryParams>(SEARCH_INGREDIENTS, { variables: { search } })

  if (error) {
    console.error(error)
    return <p>Error :(</p>
  }

  const selectedIds = ingredients.map(i => i.id)
  const filteredIngredients = data ? data.ingredients.filter(ingredient => selectedIds.indexOf(ingredient.id) < 0) : []

  return <>
    <SpacedContainer>
      <TextField label="Search ingredients" variant="outlined" fullWidth disabled={loading || !!error} value={search} onChange={e => setSearch(e.target.value)} />
    </SpacedContainer>
    <SpacedContainer>
      Available: {filteredIngredients.map(ingredient => <Chip key={'a-' + ingredient.id} label={ingredient.name} onClick={() => addIngredient(ingredient)} />)}
    </SpacedContainer>
    <SpacedContainer>
      Selected: {ingredients.map(ingredient => <Chip key={'s-' + ingredient.id} label={ingredient.name} color="primary" onDelete={() => deleteIngredient(ingredient)} />)}
    </SpacedContainer>
  </>
}
