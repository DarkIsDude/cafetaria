import React from 'react'
import { Accordion, AccordionDetails, AccordionSummary, Avatar, Chip, List, ListItem, ListItemText, Typography } from '@material-ui/core'
import { Ingredient, Recipe } from '../model'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

interface RecipesQueryResult {
  recipes: Recipe[]
}

interface RecipesQueryParams {
  ingredients: number[]
}

const SEARCH_RECIPES = gql`
query Recipes($ingredients: [Int!]) {
  recipes(limit: 10, ingredients: $ingredients) {
    author
    difficulty
    name
    image
    tags {
      name
    }
    ingredients {
      name
    }
  }
}
`

const TypographyStyled = styled(Typography)`
  display: flex;
  align-items: center;
  margin-left: 1rem;
`

interface Props {
  ingredients: Ingredient[];
}

export default function Result (props: Props) {
  const { ingredients } = props
  const ingredientsIds = ingredients.map(i => i.id)
  console.info(ingredientsIds)
  const { loading, error, data } = useQuery<RecipesQueryResult, RecipesQueryParams>(SEARCH_RECIPES, { variables: { ingredients: ingredientsIds } })

  if (loading) return <h1>Loadging</h1>
  if (error) {
    console.error(error)
    return <h1>Error</h1>
  }

  return <>
    {data.recipes.map(recipe => <Accordion>
      <AccordionSummary>
        <Avatar alt="Remy Sharp" src={recipe.image} /> <TypographyStyled>{recipe.name}</TypographyStyled>
      </AccordionSummary>
      <AccordionDetails>
        <List>
          {recipe.author && <ListItem>
            <ListItemText primary={recipe.author} secondary="author" />
          </ListItem>}
          {recipe.rate && <ListItem>
            <ListItemText primary={recipe.rate} secondary="rate" />
          </ListItem>}
          {recipe.difficulty && <ListItem>
            <ListItemText primary={recipe.difficulty} secondary="difficulty" />
          </ListItem>}
          {recipe.budget && <ListItem>
            <ListItemText primary={recipe.budget} secondary="budget" />
          </ListItem>}
          {recipe.prep_time && <ListItem>
            <ListItemText primary={recipe.prep_time} secondary="prep_time" />
          </ListItem>}
          {recipe.total_time && <ListItem>
            <ListItemText primary={recipe.total_time} secondary="total_time" />
          </ListItem>}
          {recipe.people_quantity && <ListItem>
            <ListItemText primary={recipe.people_quantity} secondary="people_quantity" />
          </ListItem>}
          {recipe.author_tip && <ListItem>
            <ListItemText primary={recipe.author_tip} secondary="author_tip" />
          </ListItem>}
          {recipe.nb_comments && <ListItem>
            <ListItemText primary={recipe.nb_comments} secondary="nb_comments" />
          </ListItem>}
          {recipe.cook_time && <ListItem>
            <ListItemText primary={recipe.cook_time} secondary="cook_time" />
          </ListItem>}
          {recipe.ingredients && <ListItem>
            <ListItemText primary={recipe.ingredients.map(i => <Chip label={i.name} />)} secondary="ingredients" />
          </ListItem>}
          {recipe.tags && <ListItem>
            <ListItemText primary={recipe.tags.map(i => <Chip label={i.name} />)} secondary="tags" />
          </ListItem>}
        </List>
      </AccordionDetails>
    </Accordion>)}
  </>
}
