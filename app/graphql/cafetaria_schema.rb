# frozen_string_literal: true

# GraphQL schema definition
class CafetariaSchema < GraphQL::Schema
  query(Queries::QueryType)

  use GraphQL::Execution::Interpreter
  use GraphQL::Analysis::AST

  use GraphQL::Pagination::Connections

  max_depth 20
  max_complexity 300
  default_max_page_size 20
end
