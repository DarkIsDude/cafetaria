# frozen_string_literal: true

module Queries
  # Recipes query GraphQL
  class Recipes < Queries::BaseQuery
    type [Types::RecipeType], null: false
    description 'Retrieve some recipes'

    argument :limit, Int, required: true, default_value: 20, prepare: ->(limit, _ctx) { [limit, 50].min }
    argument :ingredients, [Int], required: false, default_value: []

    def resolve(limit:, ingredients:)
      if ingredients
        ingredients_model = []

        ingredients.each do |ingredient|
          ingredients_model << Ingredient.find(ingredient)
        end

        recipes_from_ingredients(ingredients_model)
      else
        Recipe.all.limit(limit)
      end
    end

    private

    def recipes_from_ingredients(ingredients)
      recipes = []

      ingredients.each do |ingredient|
        recipes |= ingredient.recipes
      end

      recipes
    end
  end
end
