# frozen_string_literal: true

module Queries
  # Base query GraphQL
  class BaseQuery < GraphQL::Schema::Resolver
  end
end
