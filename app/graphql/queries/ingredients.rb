# frozen_string_literal: true

module Queries
  # Ingredients query GraphQL
  class Ingredients < Queries::BaseQuery
    type [Types::IngredientType], null: false
    description 'Retrieve some ingredients'

    argument :limit, Int, required: true, default_value: 20, prepare: ->(limit, _ctx) { [limit, 50].min }
    argument :search, String, required: false, default_value: nil

    def resolve(limit:, search:)
      query = Ingredient.all
      query = Ingredient.where('name LIKE ?', "%#{search}%") if search

      query.limit(limit)
    end
  end
end
