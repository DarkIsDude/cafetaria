# frozen_string_literal: true

module Queries
  # all queries in GraphQL
  class QueryType < Types::BaseObject
    field :ingredients, resolver: Queries::Ingredients
    field :recipes, resolver: Queries::Recipes
  end
end
