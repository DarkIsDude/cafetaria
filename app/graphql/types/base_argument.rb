# frozen_string_literal: true

module Types
  # Base argument graphql
  class BaseArgument < GraphQL::Schema::Argument
  end
end
