# frozen_string_literal: true

module Types
  # Recipe type graphql
  class RecipeType < Types::BaseObject
    graphql_name 'Recipe'
    description 'A recipe'

    field :id, Int, null: false
    field :name, String, null: false
    field :author, String, null: true
    field :rate, Float, null: true
    field :difficulty, String, null: true
    field :budget, String, null: true
    field :prep_time, String, null: true
    field :total_time, String, null: true
    field :people_quantity, Int, null: true
    field :author_tip, String, null: true
    field :image, String, null: true
    field :nb_comments, Int, null: true
    field :cook_time, String, null: true

    field :ingredients, [Types::IngredientType], null: true
    field :tags, [Types::TagType], null: true
  end
end
