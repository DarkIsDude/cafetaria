# frozen_string_literal: true

module Types
  # Base object graphql
  class BaseObject < GraphQL::Schema::Object
    field_class Types::BaseField
  end
end
