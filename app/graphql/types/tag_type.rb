# frozen_string_literal: true

module Types
  # Tag type graphql
  class TagType < Types::BaseObject
    graphql_name 'Tag'
    description 'A tag'

    field :id, Int, null: false
    field :name, String, null: false
  end
end
