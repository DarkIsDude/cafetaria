# frozen_string_literal: true

module Types
  # Ingredient type graphql
  class IngredientType < Types::BaseObject
    graphql_name 'Ingredient'
    description 'An ingredient'

    field :id, Int, null: false
    field :name, String, null: false
  end
end
