# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
namespace :importer do
  desc 'Import and init database from a JSON file'
  task :from_json, [:path] => [:environment] do |_task, args|
    path = args[:path]
    puts "Import from #{path}"

    line_counter = 0

    File.foreach(path) do |line|
      if line != '' && line != "\n" && line != "\r\n"
        data = JSON.parse line

        puts "#{line_counter}: Recipe: #{data['name']}"

        recipe = Recipe.new
        recipe.author = data['author']
        recipe.rate = data['rate']
        recipe.difficulty = data['difficulty']
        recipe.budget = data['budget']
        recipe.prep_time = data['prep_time']
        recipe.total_time = data['total_time']
        recipe.people_quantity = data['people_quantity']
        recipe.author_tip = data['author_tip']
        recipe.name = data['name']
        recipe.image = data['image']
        recipe.nb_comments = data['nb_comments']
        recipe.cook_time = data['cook_time']

        data['ingredients'].each do |ingredient|
          model = Ingredient.new

          if Ingredient.exists?(name: ingredient)
            model = Ingredient.find_by(name: ingredient)
          else
            model.name = ingredient
            model.save
          end

          recipe.ingredients << model
        end

        data['tags'].each do |tag|
          model = Tag.new

          if Tag.exists?(name: tag)
            model = Tag.find_by(name: tag)
          else
            model.name = tag
            model.save
          end

          recipe.tags << model
        end

        recipe.save
      end

      line_counter += 1
    end
  end
end
# rubocop:enable Metrics/BlockLength
